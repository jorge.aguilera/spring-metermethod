package com.example.demo;

import com.puravida.metermethod.MeterMethod;
import org.springframework.stereotype.Component;

@Component
public class GreetingService {

    private String greetings = "Greetings from";

    @MeterMethod("greetings,msg")
    public String indexMethod(String msg){
        return greetings+" "+msg;
    }
}
