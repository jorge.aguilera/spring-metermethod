package com.puravida.metermethod;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Around;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

@Aspect
@Component
public class MeterMethodAspect {

    @Around(value = "@annotation(meterMethod)", argNames = "pjp,meterMethod")
    public Object handle(ProceedingJoinPoint pjp, MeterMethod meterMethod) throws Throwable {
        Logger log = LoggerFactory.getLogger(pjp.getTarget().getClass());
        long start = System.currentTimeMillis();
        try {
            Object retVal = pjp.proceed();
            return retVal;
        }finally {
            long end = System.currentTimeMillis();
            MDC.put("className", pjp.getTarget().getClass().getName());
            MDC.put("methodName", pjp.getSignature().getName());
            MDC.put("duration", ""+(end-start));

            log.info(pjp.getSignature().getName()+" duration "+(end-start)+" ms");

            MDC.remove("className");
            MDC.remove("methodName");
            MDC.remove("duration");
        }
    }

}
